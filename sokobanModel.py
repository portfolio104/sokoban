
####################################
HEIGHT_LVL1=7 #longueur
WEIGHT_LVL1=7#largeur

MUR_LVL1=[
[0,0],[0,1],[0,2],[0,3],
[1,0],[1,3],
[2,0],[2,3],[2,4],[2,5],
[3,0],[3,5],
[4,0],[4,5],
[5,0],[5,3],[5,4],[5,5],
[6,0],[6,1],[6,2],[6,3]
]

SOCLE_LVL1=[
[1,2],
[3,1]
]
OBJET_LVL1=[
[3,1],
[4,3]
]
PERSO_LVL1=[3,2]
#####################################

HEIGHT_LVL2=7 #longueur
WEIGHT_LVL2=7 #largeur

MUR_LVL2=[
[0,0],[0,1],[0,2],[0,3],[0,4],
[1,0],[1,4],[1,5],
[2,0],[2,5],
[3,0],[3,1],[3,5],
[4,1],[4,2],[4,5],
[5,2],[5,3],[5,5],
[6,3],[6,4],[6,5]
]

SOCLE_LVL2=[
[1,1],
[5,4]
]
OBJET_LVL2=[
[2,2],[2,3]
]
PERSO_LVL2=[2,1]

#####################################

HEIGHT_LVL3=7 #longueur
WEIGHT_LVL3=7 #largeur

MUR_LVL3=[
[0,0],[0,1],[0,2],[0,3],[0,4],[0,5],[0,6],
[1,0],[1,6],
[2,0],[2,2],[2,4],[2,6],
[3,0],[3,6],
[4,0],[4,4],[4,5],[4,6],
[5,0],[5,1],[5,2],[5,3],[5,4]
]

SOCLE_LVL3=[
[3,1],[3,4]
]
OBJET_LVL3=[
[3,3],[3,4]
]
PERSO_LVL3=[3,5]

####################################Niveau moyen 

HEIGHT_LVL4=8#longueur
WEIGHT_LVL4=8#largeur

MUR_LVL4=[
[0,0],[0,1],[0,2],[0,3],[0,4],[0,5],[0,6],[0,7],
[1,0],[1,7],
[2,0],[2,7],
[3,0],[3,7],
[4,0],[4,1],[4,2],[4,3],[4,4],[4,7],
[5,4],[5,5],[5,6],[5,7]
]

SOCLE_LVL4=[
[2,2],[2,3],[2,4]
]
OBJET_LVL4=[
[2,3],[2,4],[2,5]
]
PERSO_LVL4=[2,6]


#####################################

HEIGHT_LVL5=8 #longueur
WEIGHT_LVL5=8 #largeur

MUR_LVL5=[
[0,0],[0,1],[0,2],[0,3],[0,4],[0,5],[0,6],
[1,0],[1,6],
[2,0],[2,4],[2,6],[2,7],
[3,0],[3,2],[3,7],
[4,0],[4,5],[4,7],
[5,0],[5,1],[5,2],[5,3],[5,7],
[6,4],[6,5],[6,6],[6,7]
]

SOCLE_LVL5=[
[1,1],[1,5],
[3,5]
]
OBJET_LVL5=[
[2,3],
[3,4],
[4,4]
]
PERSO_LVL5=[1,4]

####################################

HEIGHT_LVL6=8 #longueur
WEIGHT_LVL6=8 #largeur

MUR_LVL6=[
[0,1],[0,2],[0,3],[0,4],[0,5],
[1,0],[1,1],[1,5],
[2,0],[2,3],[2,5],
[3,0],[3,5],[3,6],
[4,0],[4,6],
[5,0],[5,1],[5,3],[5,6],
[6,1],[6,5],[6,6],
[7,1],[7,2],[7,3],[7,4],[7,5]
]

SOCLE_LVL6=[
[3,2],
[4,2],
[6,2]
]
OBJET_LVL6=[
[2,2],
[4,2],
[5,4]
]
PERSO_LVL6=[3,4]
#####################################

HEIGHT_LVL7=10 #longueur
WEIGHT_LVL7=10 #largeur

MUR_LVL7=[
[0,2],[0,3],[0,4],[0,5],[0,6],
[1,0],[1,1],[1,2],[1,6],
[2,0],[2,6],
[3,0],[3,1],[3,2],[3,6],
[4,0],[4,2],[4,3],[4,6],
[5,0],[5,2],[5,6],[5,7],
[6,0],[6,7],
[7,0],[7,7],
[8,0],[8,1],[8,2],[8,3],[8,4],[8,5],[8,6],[8,7]
]

SOCLE_LVL7=[
[2,1],
[3,5],
[4,1],
[5,4],
[6,3],[6,6],
[7,4]
]
OBJET_LVL7=[
[2,3],
[3,4],
[4,4],
[6,1],[6,3],[6,4],[6,5],
]
PERSO_LVL7=[2,2]

########################################
HEIGHT_LVL8=10 #longueur
WEIGHT_LVL8=10 #largeur

MUR_LVL8=[
[0,0],[0,1],[0,2],[0,3],[0,4],
[1,0],[1,4],
[2,0],[2,4],[2,6],[2,7],[2,8],
[3,0],[3,4],[3,6],[3,8],
[4,0],[4,1],[4,2],[4,4],[4,5],[4,6],[4,8],
[5,1],[5,2],[5,8],
[6,1],[6,5],[6,8],
[7,1],[7,5],[7,6],[7,7],[7,8],
[8,1],[8,2],[8,3],[8,4],[8,5]
]

SOCLE_LVL8=[
    [3,7],
    [4,7],
    [5,7]
]
OBJET_LVL8=[
    [2,2],[2,3],
    [3,2],


]
PERSO_LVL8=[1,3]
#############################################"
HEIGHT_LVL9=10
WEIGHT_LVL9=10

MUR_LVL9=[
[0,0],[0,1],[0,2],[0,3],[0,4],[0,5],[0,6],[0,7],[0,8],
[1,0],[1,8],
[2,0],[2,8],
[3,0],[3,1],[3,3],[3,4],[3,5],[3,8],
[4,1],[4,7],[4,8],
[5,1],[5,7],[5,8],
[6,1],[6,6],[6,7],
[7,1],[7,6],
[8,1],[8,4],[8,5],[8,6],
[9,1],[9,2],[9,3],[9,4],
]
SOCLE_LVL9=[
[1,1],[1,3],
[2,1],[2,3],[2,5],
]
OBJET_LVL9=[
[2,2],
[4,4],
[5,3],[5,4],
[6,4],
]
PERSO_LVL9=[3,6]



class sokobanModel:

    def __init__(self,niveau):
        
        ############# CREATION TABLE DE JEU ##############
        # 0 represente vide
        # 1 represente le personnage
        # 2 represente les bordures
        # 3 represente un cube
        # 4 represente les endroit d'arrivée 
        # 5 represente un cube sur une arrivée
        # 6 represente un joueur sur une arrivée
        self.__niveau=niveau
        self.__plateau = None

        self.__ligne = -1
        self.__colonne = -1
        self.__tmp=0
        self.tabModel=[]
        if(niveau!=0):
            self.createTabJeu(self.__niveau)
        else:
            self.generateTabJeu()

        self.__view = None
        

    def setView(self , view):
        self.__view=view

    def getSocle(self):
        return self.__SOCLE

    def generateTabNull(self,h,w):
        L=[]
        for i in range(h):
            l=[]
            for j in range(w):
                l.append(0)
            L.append(l)
        return L

    def createTabJeu(self,niveau):
        self.__HEIGHT=eval('HEIGHT_LVL'+str(niveau))
        print(self.__HEIGHT)
        self.__WEIGHT=eval('WEIGHT_LVL'+str(niveau))
        self.__PERSO=eval('PERSO_LVL'+str(niveau))
        self.__SOCLE=eval('SOCLE_LVL'+str(niveau))
        self.__OBJET=eval('OBJET_LVL'+str(niveau))
        self.__MUR=eval('MUR_LVL'+str(niveau))
        print(eval('MUR_LVL'+str(niveau)))
        self.__plateau=self.generateTabNull(self.__HEIGHT,self.__WEIGHT)
        for coor in self.__MUR:
            self.__plateau[coor[0]][coor[1]]=2
        for coor in self.__SOCLE:
            self.__plateau[coor[0]][coor[1]]=4
        for coor in self.__OBJET:
            if self.__plateau[coor[0]][coor[1]]!=4:
                self.__plateau[coor[0]][coor[1]]=3
            else:
                self.__plateau[coor[0]][coor[1]]=5
        self.__ligne=self.__PERSO[0]
        self.__colonne=self.__PERSO[1]
        self.__plateau[self.__ligne][self.__colonne]=1

        self.tabModel.append(self.__plateau)

    

    def afficheTab(self) :
        for i in range(len(self.__plateau)):
            chaine = ""
            for j in self.__plateau[i] :
                chaine += "" + str(j)

         
            

    def getPlateau(self):
        return self.__plateau

    def setPlateau(self,plateau):
        self.__plateau=plateau
        print(self.__plateau)

    def setPosJoueur(self,ligne,colonne):
        self.__ligne=ligne
        self.__colonne=colonne

            
    def getPlaceJoueur(self):      
        return (self.__ligne,self.__colonne)


    def nbSocle(self):
        return len(self.__SOCLE)

    def restart(self):
        self.createTabJeu(self.__niveau)
        self.__conteur=0
        
        for i in self.__SOCLE:
            if self.__plateau[i[0]][i[1]]==5:
                self.__conteur+=1
        self.__view.setPlateau(self.__plateau)
        self.afficheTab()

            
         
