from random import randint
from PyQt5.QtWidgets import QMainWindow,QMenu,QGridLayout,QLabel,QAction,QWidget
from PyQt5.QtGui import QPixmap, QMovie
from PyQt5.QtMultimedia import QSound



class sokobanView(QMainWindow):
    def __init__(self):
        super().__init__()
        self.__tabTaille = [600,700,800,900]
        
        self._createMenuBar()
        self.__window = QWidget()
        self.setCentralWidget(self.__window)
        self.__gridLayout = QGridLayout()
        self.__window.setLayout(self.__gridLayout)

        self.__model = None
        self.__controller = None
        self.__size=None

        self.__gridLayout.setSpacing(0)
        self.wallSound= QSound("hmm.wav")
        self.caisseSound= QSound("iya.wav")
    
        self.show()
        # On divise la fenetre en 8 cases donc largeurFentre/8 et longueurFenetre/8 
        # Sur l'affichage, chaque objet represente donc un cube de largeurFentre/8 et longueurFenetre/8 
        # Pour placer le joueur, on fait (largeurFenetre/8)*
    
    def useSoundWall(self):
        self.wallSound.play()

    def useSoundCaisse(self):
        self.caisseSound.play()

    def setModel(self,model,niveau):
        self.__model = model
        self.__plateau = self.__model.getPlateau()
        self.__taille = self.__tabTaille[niveau] # le niveau 0 est le niveau aléatoire
        self.createView()

        for i in range(8):
            for j in range(8):
                self.__gridLayout.addWidget(QWidget(), i, j)
    def setPlateau(self,plateau):
        self.__plateau=plateau
        self.__window = QWidget()
        self.setCentralWidget(self.__window)
        self.__gridLayout = QGridLayout()
        self.__window.setLayout(self.__gridLayout)
        self.__gridLayout.setSpacing(0)
        self.createView()

        
    def setController(self,controller):
        self.__controller = controller

    def initUI(self,size,niveau):
        self.__size= size
        
        self.top =(self.__size[0]-self.__tabTaille[niveau])//2
        self.left=(self.__size[1]-self.__tabTaille[niveau])//2
        self.setGeometry(self.left,self.top,self.__tabTaille[niveau],self.__tabTaille[niveau])
        
    def _createMenuBar(self):
        menuBar = self.menuBar()
        gameMenu = QMenu("&Jeu", self)
        menuBar.addMenu(gameMenu)


        restart = QAction(self)
        restart.setText("&Restart")
        gameMenu.addAction(restart)
        restart.triggered.connect(self.restart)

        exitProgram = QAction(self)
        exitProgram.setText("&Quit")
        gameMenu.addAction(exitProgram)
        exitProgram.triggered.connect(self.close)

        
    def createView(self):
        # 0 represente vide
        # 1 represente le personnage
        # 2 represente les bordures
        # 3 represente un cube
        # 4 represente les endroit d'arrivée 
        # 5 represente un cube sur une arrivée
        # 6 represente un joueur sur une arrivée
        for i in range(len(self.__plateau)):
            for j in range(len(self.__plateau[i])):
               
                if self.__plateau[i][j]==0:
                    imageSol = QPixmap("./sol.png")  
                    sol = QLabel()
                    sol.setPixmap(imageSol.scaled(self.__taille/len(self.__plateau[0]),self.__taille/len(self.__plateau)))
                    self.__gridLayout.addWidget(sol,i,j)

                elif self.__plateau[i][j]== 1:
                    
                    imagePerso =QPixmap("./mario.png")  
                    imageSol = QPixmap("./sol.png")   

                    perso = QLabel()
                    perso.setStyleSheet("background-image:url(./sol.png)")
                    perso.setPixmap(imagePerso.scaled(self.__taille/len(self.__plateau[0]),self.__taille/len(self.__plateau)))
                    self.__gridLayout.addWidget(perso, i, j)

                elif self.__plateau[i][j]==2:
                    imageBordure = QPixmap("./block.png")  
                    bordure = QLabel()
                    bordure.setPixmap(imageBordure.scaled(self.__taille/len(self.__plateau[0]),self.__taille/len(self.__plateau)))
                    self.__gridLayout.addWidget(bordure, i, j)

                
                elif self.__plateau[i][j]==3:
                    imageCube = QPixmap("./cube.png")   
                    cube = QLabel()
                    cube.setPixmap(imageCube.scaled(self.__taille/len(self.__plateau[0]),self.__taille/len(self.__plateau)))
                    self.__gridLayout.addWidget(cube,i,j)

                elif self.__plateau[i][j]==4:

                    arrivee = QLabel()
                    imageSocle = QPixmap("./socle.png") 
                    arrivee.setPixmap(imageSocle.scaled(self.__taille/len(self.__plateau[0]),self.__taille/len(self.__plateau)))

                    self.__gridLayout.addWidget(arrivee,i,j)

                elif self.__plateau[i][j]==5:
                    imageCubeActif = QPixmap("./cubeActif.png")   
                    cubeActif = QLabel()
                    cubeActif.setPixmap(imageCubeActif.scaled(self.__taille/len(self.__plateau[0]),self.__taille/len(self.__plateau)))
                    self.__gridLayout.addWidget(cubeActif,i,j)
                        
                else : 
                    imagePerso =QPixmap("./mario.png")  
                    imageSocle = QPixmap("./socle.png")   

                    persoC = QLabel()
                    persoC.setStyleSheet("background-image:url(./socle.png)")
                    persoC.setPixmap(imagePerso.scaled(self.__taille/len(self.__plateau[0]),self.__taille/len(self.__plateau)))
                    self.__gridLayout.addWidget(perso, i, j)

                    

    def updateView(self,tabCase) :
        
            for i in range(len(tabCase)):

                if self.__plateau[tabCase[i][0]][tabCase[i][1]]==0:
                    imageSol =QPixmap("./sol.png")  
                    self.__gridLayout.itemAtPosition(tabCase[i][0],tabCase[i][1]).widget().clear()
                    self.__gridLayout.itemAtPosition(tabCase[i][0],tabCase[i][1]).widget().clear()
                    self.__gridLayout.itemAtPosition(tabCase[i][0],tabCase[i][1]).widget().setPixmap(imageSol.scaled(self.__taille/len(self.__plateau[0]),self.__taille/len(self.__plateau))) 

                elif self.__plateau[tabCase[i][0]][tabCase[i][1]]==1:
                    imagePerso =QPixmap("./mario.png")  
                    self.__gridLayout.itemAtPosition(tabCase[i][0],tabCase[i][1]).widget().clear()
                    self.__gridLayout.itemAtPosition(tabCase[i][0],tabCase[i][1]).widget().setStyleSheet("background-image:url(./sol.png)")
                    self.__gridLayout.itemAtPosition(tabCase[i][0],tabCase[i][1]).widget().setPixmap(imagePerso.scaled(self.__taille/len(self.__plateau[0]),self.__taille/len(self.__plateau))) 


                elif self.__plateau[tabCase[i][0]][tabCase[i][1]]==2:
                    imageBordure = QPixmap("./block.png")  
                    self.__gridLayout.itemAtPosition(tabCase[i][0],tabCase[i][1]).widget().clear()
                    self.__gridLayout.itemAtPosition(tabCase[i][0],tabCase[i][1]).widget().setPixmap(imageBordure.scaled(self.__taille/len(self.__plateau[0]),self.__taille/len(self.__plateau))) 

                    
                elif self.__plateau[tabCase[i][0]][tabCase[i][1]]==3:
                    imageCube=QPixmap("./cube.png")  
                    self.__gridLayout.itemAtPosition(tabCase[i][0],tabCase[i][1]).widget().clear()
                    self.__gridLayout.itemAtPosition(tabCase[i][0],tabCase[i][1]).widget().setPixmap(imageCube.scaled(self.__taille/len(self.__plateau[0]),self.__taille/len(self.__plateau))) 

                elif self.__plateau[tabCase[i][0]][tabCase[i][1]]==4:
                    self.__gridLayout.itemAtPosition(tabCase[i][0],tabCase[i][1]).widget().clear()
                    imageSocle = QPixmap("./socle.png") 

                    self.__gridLayout.itemAtPosition(tabCase[i][0],tabCase[i][1]).widget().setPixmap(imageSocle.scaled(self.__taille/len(self.__plateau[0]),self.__taille/len(self.__plateau))) 

                elif self.__plateau[tabCase[i][0]][tabCase[i][1]]==5:
                    imageCubeActif = QPixmap("./cubeActif.png") 
                    self.__gridLayout.itemAtPosition(tabCase[i][0],tabCase[i][1]).widget().clear()
                    self.__gridLayout.itemAtPosition(tabCase[i][0],tabCase[i][1]).widget().setPixmap(imageCubeActif.scaled(self.__taille/len(self.__plateau[0]),self.__taille/len(self.__plateau))) 

                else : 
                    imagePerso =QPixmap("./mario.png")  
                    imageSocle = QPixmap("./socle.png")   
                    self.__gridLayout.itemAtPosition(tabCase[i][0],tabCase[i][1]).widget().clear()
                    self.__gridLayout.itemAtPosition(tabCase[i][0],tabCase[i][1]).widget().setStyleSheet("background-image:url(./socle.png)")
                    self.__gridLayout.itemAtPosition(tabCase[i][0],tabCase[i][1]).widget().setPixmap(imagePerso.scaled(self.__taille/len(self.__plateau[0]),self.__taille/len(self.__plateau))) 

                    
        


    def keyPressEvent(self, event):
        gauche=16777234
        key = event.key()
        if(event.isAutoRepeat()==False):
            if(key==gauche):
                self.__controller.play(1, -1)
            elif(key==gauche+1):
                self.__controller.play(0, -1)
            elif(key==gauche+2):
                self.__controller.play(1, 1)
            elif(key==gauche+3):
                self.__controller.play(0, 1)
                    
    def setVictoire(self):
        """
        change la fenetre de jeu et affiche victoire
        """

        self.__window = QWidget()
        self.setCentralWidget(self.__window)
        self.__gridLayout = QGridLayout()
        self.__window.setLayout(self.__gridLayout)   
        imageVictoire = QMovie("./victoire.gif")   
        bloc= QLabel()
        bloc.setMovie(imageVictoire)
        imageVictoire.start()
        self.__gridLayout.addWidget(bloc,0,0)  


    def setDefaite(self):
        """
        change la fenetre de jeu et affiche victoire
        """

        self.__window = QWidget()
        self.setCentralWidget(self.__window)
        self.__gridLayout = QGridLayout()
        self.__window.setLayout(self.__gridLayout)      
        imageVictoire = QPixmap("./echec.jpg")  
        victoire = QLabel()
        victoire.setPixmap(imageVictoire) 
        self.__gridLayout.addWidget(victoire, 0, 0)
    def clear(self) :

        self.__gridLayout = QGridLayout()
        self.__window.setLayout(self.__gridLayout)


    def restart(self,model):
        self.__controller.restart()
