class sokobanController : 
    """
    Controller du sokoban
    """

    def __init__(self):
        self.__model = None
        self.__view = None
        self.s=None
        self.__plateau=[]
        self.__conteur=0
        self.__tmp = 0
        self.__victoire=False
        self.__defaite=False

    def conteur(self):
        res = 0
        for i in range(len(self.__plateau)):
            for j in range(len(self.__plateau[0])):
                if self.__plateau[i][j]==5:
                    res+=1
        
        self.__conteur=res

    def setModel(self,model):
        self.__model=model
        self.s=self.__model.getSocle()
    
    def setView(self,view):
        self.__view=view 

    def play(self,endroitModif,valeur):
        """
        Ici, on voit si l'evenement clavier pour faire bouger le joueur est possible
        on donne en parametre endroitModif si l'action se fait sur la colonne ou sur une ligne
        et en valeur si on ajoute 1 ou si on retire 1
        On definit une varible victoire qui donne dit a la vue d'afficher vicoire avec self.__view.setVictoire() 
        On donne egalement au modele l'endroit a modifier en lui donat endroitMOdif et valeur si le deplacement est possible

        """
        self.__plateau = self.__model.getPlateau() 

        joueur = self.__model.getPlaceJoueur()
        ligne = joueur[0]
        colonne = joueur[1]

        if endroitModif == 0:
            if self.__plateau[ligne+valeur][colonne] != 2 :
                
                tabCase=[]
                tabCase.append((ligne,colonne)) # on donne la place du joueur 

       
                tmp= self.__plateau[ligne + valeur][colonne]
                if tmp==0:
                    self.__plateau[ligne + valeur][colonne] = 1
                    self.__plateau[ligne][colonne] = self.__tmp
                    self.__tmp=tmp
                    ligne = ligne + valeur
                    tabCase.append((ligne,colonne))
                if tmp==4:  
                    
                    self.__plateau[ligne + valeur][colonne] = 6
                    self.__plateau[ligne][colonne] = self.__tmp
                    self.__tmp=tmp
                    ligne = ligne + valeur
                    tabCase.append((ligne,colonne))

                if tmp==3:
                    self.__view.useSoundCaisse()
                    if self.__plateau[ligne+valeur*2][colonne]==4:
                        self.__plateau[ligne + valeur][colonne] = 1 
                        self.__plateau[ligne+valeur*2][colonne]=5
                        self.__plateau[ligne][colonne] = self.__tmp
                        self.__tmp=0
                        ligne = ligne + valeur
                        self.__conteur+=1
                        tabCase.append((ligne,colonne)) # on ajoute la nouvel position du joueur
                        tabCase.append((ligne + valeur,colonne)) # on donne la place du cube 
                        tabCase.append((ligne + valeur*2,colonne)) # on donne la nouvelle place du cube 
                    elif self.__plateau[ligne+valeur*2][colonne]!=2 and self.__plateau[ligne+valeur*2][colonne]!=3 and self.__plateau[ligne+valeur*2][colonne]!=5:
                        self.__plateau[ligne + valeur][colonne] = 1
                        self.__plateau[ligne+valeur*2][colonne]=tmp
                        self.__plateau[ligne][colonne] = self.__tmp
                        self.__tmp=0
                        ligne = ligne + valeur
                        tabCase.append((ligne,colonne)) # on ajoute la nouvel position du joueur
                        tabCase.append((ligne + valeur,colonne)) # on donne la place du cube 
                        tabCase.append((ligne + valeur*2,colonne)) # on donne la nouvelle place du cube     
                if tmp==5:
                    self.__view.useSoundCaisse()
                    if self.__plateau[ligne+valeur*2][colonne]!=2 and self.__plateau[ligne+valeur*2][colonne]!=5 and self.__plateau[ligne+valeur*2][colonne]!=3:
                        if self.__plateau[ligne+valeur*2][colonne]!=4:
                            self.__plateau[ligne + valeur][colonne] = 6
                            self.__plateau[ligne+valeur*2][colonne]=3
                            self.__plateau[ligne][colonne] = self.__tmp
                            self.__tmp=4
                            ligne = ligne + valeur
                            self.__conteur-=1
                        else:
                            self.__plateau[ligne + valeur][colonne] = 6
                            self.__plateau[ligne+valeur*2][colonne]=5
                            self.__plateau[ligne][colonne] = self.__tmp
                            self.__tmp=4
                            ligne = ligne + valeur

                        tabCase.append((ligne,colonne)) # on ajoute la nouvel position du joueur
                        tabCase.append((ligne + valeur,colonne)) # on donne la place du cube 
                        tabCase.append((ligne + valeur*2,colonne)) # on donne la nouvelle place du cube 


                for i in range(len(self.__plateau)):
                    for j in range(len(self.__plateau[0])):
                        if self.__plateau[i][j]==3:
                            if (self.__plateau[i][j+1]==2  )and (self.__plateau[i+1][j]==2 ):
                                self.__defaite=True
                            elif (self.__plateau[i][j+1]==2 )  and (self.__plateau[i-1][j]==2 ):
                                self.__defaite=True
                            elif (self.__plateau[i][j-1]==2  )and (self.__plateau[i+1][j]==2 ):
                                self.__defaite=True
                            elif (self.__plateau[i][j-1]==2  ) and (self.__plateau[i-1][j]==2 ):
                                self.__defaite=True
                    
                self.conteur()        
                
                self.__model.setPlateau(self.__plateau)
                self.__model.setPosJoueur(ligne,colonne)
                self.__view.updateView(tabCase)
                self.testVictoire()
                if self.__defaite == True:
                    self.__view.setDefaite()
        
            elif  self.__plateau[ligne+valeur][colonne] == 2 :
                self.__view.useSoundWall()

                
        else :
            if self.__plateau[ligne][colonne+valeur] != 2 :
                tabCase=[]
                tabCase.append((ligne,colonne)) # on donne la place du joueur 
                tmp=self.__plateau[ligne][colonne + valeur]
                if  tmp==0:
                    self.__plateau[ligne][colonne + valeur] = 1
                    self.__plateau[ligne][colonne] = self.__tmp
                    self.__tmp=tmp
                    colonne = colonne + valeur
                    tabCase.append((ligne,colonne))
                if tmp==4 :
                    
                    self.__plateau[ligne][colonne + valeur] = 6
                    self.__plateau[ligne][colonne] = self.__tmp
                    self.__tmp=tmp
                    colonne = colonne + valeur
                    tabCase.append((ligne,colonne))


                if tmp==3:
                    self.__view.useSoundCaisse()
                    if self.__plateau[ligne][colonne+valeur*2]==4:
                        self.__plateau[ligne][colonne + valeur] = 1
                        self.__plateau[ligne][colonne+valeur*2]=5
                        self.__plateau[ligne][colonne] = self.__tmp
                        self.__tmp=0
                        colonne = colonne + valeur
                        self.__conteur+=1
                        tabCase.append((ligne,colonne)) # on ajoute la nouvel position du joueur
                        tabCase.append((ligne,colonne + valeur)) # on donne la place du cube 
                        tabCase.append((ligne ,colonne + valeur*2)) # on donne la nouvelle place du cube 
                    elif self.__plateau[ligne][colonne+valeur*2]!=2 and self.__plateau[ligne][colonne+valeur*2]!=3 and self.__plateau[ligne][colonne+valeur*2]!=5:

                        self.__plateau[ligne][colonne + valeur] = 1
                        self.__plateau[ligne][colonne+valeur*2]=tmp
                        self.__plateau[ligne][colonne] = self.__tmp
                        self.__tmp=0
                        colonne = colonne + valeur
                        tabCase.append((ligne,colonne))
                        tabCase.append((ligne,colonne)) # on ajoute la nouvel position du joueur
                        tabCase.append((ligne,colonne + valeur)) # on donne la place du cube 
                        tabCase.append((ligne ,colonne + valeur*2)) # on donne la nouvelle place du cube 
                        

                if tmp==5:
                    self.__view.useSoundCaisse()
                    if self.__plateau[ligne][colonne + valeur*2]!=2 and self.__plateau[ligne][colonne + valeur*2]!=3 and self.__plateau[ligne][colonne + valeur*2]!=5:
                        if self.__plateau[ligne][colonne + valeur*2]!=4 :
                            self.__plateau[ligne][colonne + valeur] = 6
                            self.__plateau[ligne][colonne+valeur*2]=3
                            self.__plateau[ligne][colonne] = self.__tmp
                            self.__tmp=4
                            colonne = colonne + valeur
                            self.__conteur-=1
                        else :
                            self.__plateau[ligne][colonne + valeur] = 6
                            self.__plateau[ligne][colonne+valeur*2]=5
                            self.__plateau[ligne][colonne] = self.__tmp
                            self.__tmp=4
                            colonne = colonne + valeur
                        tabCase.append((ligne,colonne))
                        tabCase.append((ligne,colonne)) # on ajoute la nouvel position du joueur
                        tabCase.append((ligne,colonne + valeur)) # on donne la place du cube 
                        tabCase.append((ligne ,colonne + valeur*2)) # on donne la nouvelle place du cube




                
                
                for i in range(len(self.__plateau)):
                    for j in range(len(self.__plateau[0])):
                        if self.__plateau[i][j]==3:
                            if (self.__plateau[i][j+1]==2  )and (self.__plateau[i+1][j]==2 ):
                                self.__defaite=True
                            elif (self.__plateau[i][j+1]==2 )  and (self.__plateau[i-1][j]==2 ):
                                self.__defaite=True
                            elif (self.__plateau[i][j-1]==2  )and (self.__plateau[i+1][j]==2 ):
                                self.__defaite=True
                            elif (self.__plateau[i][j-1]==2  ) and (self.__plateau[i-1][j]==2 ):
                                self.__defaite=True

            
                self.conteur()        
                
                self.__model.setPlateau(self.__plateau)
                self.__model.setPosJoueur(ligne,colonne)
                self.__view.updateView(tabCase)
                self.testVictoire()
                if self.__defaite == True:
                    self.__view.setDefaite()

                print(tabCase)

            else:
                self.__view.useSoundWall()

   

    def testVictoire(self):
        if self.__conteur==self.__model.nbSocle():
            self.__victoire=True
        
        if self.__victoire==True:
            self.__view.setVictoire()

    

    def restart(self):
        self.__view.clear()
        self.__model.restart()
        self.__victoire=False
        self.__defaite=False

