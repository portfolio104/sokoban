from sokobanModel import sokobanModel
from sokobanView import sokobanView
from sokobanController import sokobanController
import sys
import random
from PyQt5.QtWidgets import QApplication,  QPushButton,  QWidget,QGridLayout

class main(QWidget):
    def __init__(self) :
        super().__init__()
        

        screen=app.primaryScreen()
        
        
        size=screen.size()
        self.size_screen=[size.height(),size.width()]

        self.height=120
        self.width=200
        self.top =(self.size_screen[0]-self.height)//2
        self.left=(self.size_screen[1]-self.width)//2

        self.title = "Menu"
        self.__gridLayout = QGridLayout()
        self.btn1 = QPushButton("Débutant",self)
        self.btn2 = QPushButton("Intermediaire",self)
        self.btn3 = QPushButton("Expert",self)
        self.btn4 = QPushButton("Niveau Aleatoire",self)
        self.niveau=None
        self.setLayout(self.__gridLayout)
        self.initUI()
        self.show()

    def initUI(self):
        self.setGeometry(self.left,self.top,self.width,self.height)
        self.btn1.clicked.connect(self.on_click1)
        self.btn1.adjustSize()
        self.__gridLayout.addWidget(self.btn1)
        
        self.btn2.clicked.connect(self.on_click2)
        self.btn2.adjustSize()
        self.__gridLayout.addWidget(self.btn2)

        self.btn3.clicked.connect(self.on_click3)
        self.btn3.adjustSize()
        self.__gridLayout.addWidget(self.btn3)

        self.btn4.clicked.connect(self.on_click4)
        self.btn4.adjustSize()
        #self.btn4.setPalette(QtGui.QPalette(QtGui.QColor("black")))
        self.__gridLayout.addWidget(self.btn4)
        
    def generateJeu(self,n):
        o=random.randint(1+(n-1)*3,(n-1)*3+3)
        model = sokobanModel(o)
        view = sokobanView()
        controller = sokobanController()

        view.setModel(model,self.niveau)
        view.setController(controller)
        view.initUI(self.size_screen,self.niveau)

        controller.setModel(model)
        controller.setView(view)
        model.setView(view)
        self.close()
    
    def on_click1(self):
        self.niveau=1
        self.generateJeu(self.niveau)
    def on_click2(self):
        self.niveau=2
        self.generateJeu(self.niveau)
    def on_click3(self):
        self.niveau=3
        self.generateJeu(self.niveau)
    def on_click4(self):
        self.niveau=0
        self.generateJeu(self.niveau)

if __name__=='__main__':
    app = QApplication(sys.argv)
    M=main()
    sys.exit(app.exec_())
